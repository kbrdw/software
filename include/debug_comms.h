#ifndef DEBUG_COMMS_H
#define DEBUG_COMMS_H

enum {
    MIN_LEVEL = 0,
    INFO = 20,
    DEBUG = 40,
    MAX_LEVEL = 50
};

class DebugComms {
    public:
        DebugComms(int setLevel) : 
            currentVerbosityLevel(setLevel) {};
        
        /* Using __attribute__ to check if format is consistent with the printf-style format string */
        void print(int level, const char * format, ...) __attribute__ ((format (printf, 3, 4)));

        void setLevel(int newLevel);

    private:
        int currentVerbosityLevel;
        
};

extern DebugComms debugComms;

#endif