#include <stddef.h>
#include <Ticker.h>

#define DEBOUNCE_THRESHOLD_MS       100
#define REQUEST_TIMEOUT_S           180
#define END_AUTO_REJECT_TIMEOUT_S   10
#define MESSAGE_DELIVERY_TIMEOUT_S  1.5F

extern Ticker toggleLedTimer;
extern Ticker autoRejectTimer;
extern Ticker awaitingResponseTimer;
extern Ticker messageDeliveryTimeoutTimer;


extern bool reqReceived;
extern bool reqAccepted;
extern bool reqCancelled;
extern bool reqRejected;
extern bool msgDeliveryConfirmed;

extern bool endOfAutoReject;
extern bool endOfAwaiting;
extern bool switchSwitched;
extern bool messageDeliveryTimeout;


void toggleLed();
void clearLeds();
bool buttonPressed(int pinNumber);
void handleQuery(char* payload_a, size_t len);

void acceptRequest();
void sendRequest();
void cancelRequest();
void rejectRequest();
void confirmDelivery();

void endAutoReject();
void endAwaitingResponse();
void setDeliveryTimeout();
