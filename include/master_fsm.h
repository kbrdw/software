#ifndef MASTER_FSM_H
#define MASTER_FSM_H


enum MasterState_t {
    ST_HW_INIT,             // Hardware initialisation
    ST_WIFI_CONNECT,        // WiFi Connection setup
    ST_NETWORK_SETUP,       // Networking and MQTT setup
    ST_AVAILABLE,           // Idle, avaiable state
    ST_UNAVAILABLE,         // Idle, unavailable state
    ST_INVITATION_SENT,     // Invitation sent, but unsure of delivery
    ST_AWAITING_RESPONSE,   // Waiting for a peer response to a request
    ST_REQUEST_PENDING,     // Received a request from peer, waiting for user
    ST_REQUEST_ACCEPTED,    // Request accepted by both parties
    ST_REQUEST_FAILED,      // Request failed for technical reasons
};

extern enum MasterState_t master_fsm_state;

#endif