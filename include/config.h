#define NODEMCUV2   0
#define WITTY       1

/** Hardware pins aliases **/
#ifndef HW_TYPE
    #error "Hardware type not defined."
#endif

#if HW_TYPE == NODEMCUV2
    #define MY_ID 1
    #define REMOTE_ID 0

    #define LED_ON  LOW
    #define LED_OFF HIGH

    #define LED_RED 15          // D8
    #define LED_GREEN 12        // D6
    #define LED_RGB_R 13        // D7
    #define LED_RGB_G 2         // D4
    #define LED_RGB_B 0         // D3
    #define POS_BUTTON_PIN 14   // D5
    #define NEG_BUTTON_PIN 16   // D0
    #define AVAILABILITY_SWITCH 4 // D2 TODO: Free up for I2C

    /* I2C signals */
    #define SDA_PIN 4           // D2
    #define SCL_PIN 5           // D1

    #define LED_INFO                LED_RGB_B
    #define LED_STATUS_AVAILABLE    LED_RGB_G
    #define LED_STATUS_UNAVAILABLE  LED_RGB_R
    #define LED_REQ_ACCEPTED        LED_GREEN   // Positive actions: request, accept
    #define LED_REQ_REJECTED        LED_RED     // Negative actions: cancel, reject, clear leds

#elif HW_TYPE == WITTY
    #define MY_ID 0
    #define REMOTE_ID 1

    #define LED_ON  HIGH
    #define LED_OFF LOW

    #define LED_RED 14
    #define LED_YELLOW 15
    #define LED_BLUE 13
    #define LED_GREEN 12
    #define POS_BUTTON_PIN 0
    #define NEG_BUTTON_PIN 2
    #define AVAILABILITY_SWITCH 16

    /* I2C signals */
    #define SDA_PIN 4           // D2
    #define SCL_PIN 5           // D1 

    #define LED_INFO                LED_YELLOW
    #define LED_STATUS_AVAILABLE    LED_BLUE
    #define LED_STATUS_UNAVAILABLE  LED_BLUE
    #define LED_REQ_ACCEPTED        LED_GREEN   // Positive actions: request, accept
    #define LED_REQ_REJECTED        LED_RED     // Negative actions: cancel, reject, clear leds

#endif


/** Serial config **/
#define SERIAL_BAUD 115200

/** Mqtt config **/
#define MQTT_HOST IPAddress(52, 54, 110, 50) //io.adafruit.com
#if ASYNC_TCP_SSL_ENABLED
#define MQTT_SECURE true
#define MQTT_SERVER_FINGERPRINT {0x7e, 0x36, 0x22, 0x01, 0xf9, 0x7e, 0x99, 0x2f, 0xc5, 0xdb, 0x3d, 0xbe, 0xac, 0x48, 0x67, 0x5b, 0x5d, 0x47, 0x94, 0xd2}
#define MQTT_PORT 8883
#else
#define MQTT_PORT 1883
#endif

#define MQTT_USERNAME "lukigruszka"
#define MQTT_PASSWORD "aio_FOQr54oVcxjBroacOXTkBC2wFvsV"