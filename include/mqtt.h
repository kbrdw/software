#ifndef MQTT_H
#define MQTT_H

#include <AsyncMqttClient.h>
#include <Ticker.h>

extern AsyncMqttClient mqttClient;
extern Ticker mqttReconnectTimer;

/*** Must be declared by user **/
extern void handleQuery(char*, size_t);

void connectToMqtt();
void onMqttConnect(bool sessionPresent);
void onMqttDisconnect(AsyncMqttClientDisconnectReason reason);
void onMqttSubscribe(uint16_t packetId, uint8_t qos);
void onMqttUnsubscribe(uint16_t packetId);
void onMqttMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total);
void onMqttPublish(uint16_t packetId);

#endif