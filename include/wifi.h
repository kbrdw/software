#ifndef WIFI_H
#define WIFI_H

#include <Ticker.h>
#include <ESP8266WiFi.h>

extern WiFiEventHandler wifiConnectHandler;
extern WiFiEventHandler wifiDisconnectHandler;

extern Ticker mqttReconnectTimer;
extern void connectToMqtt();

void connectToWifi();
void onWifiConnect(const WiFiEventStationModeGotIP& event);
void onWifiDisconnect(const WiFiEventStationModeDisconnected& event);

#endif