#include <Arduino.h>

#include "core_functions.h"
#include "master_fsm.h"
#include "wifi.h"
#include "mqtt.h"
#include "debug_comms.h"

#include "config.h"

#include <AsyncMqttClient.h>
#include <Ticker.h>

enum MasterState_t master_fsm_state;
bool stateEntry = true;     /* Helper flag to indicate to the FSM that the state is being entered */


void setup() {
    master_fsm_state = ST_HW_INIT;
    debugComms.setLevel(INFO);

    pinMode(LED_INFO, OUTPUT);
    pinMode(LED_REQ_ACCEPTED, OUTPUT);
    pinMode(LED_REQ_REJECTED, OUTPUT);
    pinMode(LED_STATUS_AVAILABLE, OUTPUT);
    pinMode(LED_STATUS_UNAVAILABLE, OUTPUT);
    digitalWrite(LED_INFO, LED_OFF);
    digitalWrite(LED_REQ_ACCEPTED, LED_OFF);
    digitalWrite(LED_REQ_REJECTED, LED_OFF);
    digitalWrite(LED_STATUS_UNAVAILABLE, LED_OFF);
    digitalWrite(LED_STATUS_AVAILABLE, LED_ON);
    pinMode(POS_BUTTON_PIN, INPUT_PULLUP);
    pinMode(NEG_BUTTON_PIN, INPUT_PULLUP);
    pinMode(AVAILABILITY_SWITCH, INPUT_PULLUP);

    Serial.begin(SERIAL_BAUD);
    debugComms.print(MIN_LEVEL, "\r\n\r\n");

    wifiConnectHandler = WiFi.onStationModeGotIP(onWifiConnect);
    wifiDisconnectHandler = WiFi.onStationModeDisconnected(onWifiDisconnect);

    mqttClient.onConnect(onMqttConnect);
    mqttClient.onDisconnect(onMqttDisconnect);
    mqttClient.onSubscribe(onMqttSubscribe);
    mqttClient.onUnsubscribe(onMqttUnsubscribe);
    mqttClient.onMessage(onMqttMessage);
    mqttClient.onPublish(onMqttPublish);
    mqttClient.setServer(MQTT_HOST, MQTT_PORT);
    mqttClient.setCredentials(MQTT_USERNAME, MQTT_PASSWORD);
#if ASYNC_TCP_SSL_ENABLED
    mqttClient.setSecure(MQTT_SECURE);
    if (MQTT_SECURE) {
        mqttClient.addServerFingerprint((const uint8_t[])MQTT_SERVER_FINGERPRINT);
    }
#endif

    master_fsm_state = ST_WIFI_CONNECT;
    connectToWifi();
}

void loop() {
    switch (master_fsm_state) {
    case ST_HW_INIT:
        break;

    case ST_WIFI_CONNECT:
        break;

    case ST_NETWORK_SETUP:
        break;

    case ST_AVAILABLE:
        if (buttonPressed(POS_BUTTON_PIN)) {
            sendRequest();
            clearLeds();
            awaitingResponseTimer.once(REQUEST_TIMEOUT_S, endAwaitingResponse);
            digitalWrite(LED_INFO, LED_ON);

            stateEntry = true;
            master_fsm_state = ST_INVITATION_SENT;
        }
        else if (reqReceived){
            reqReceived = false;
            clearLeds();
            toggleLedTimer.attach_ms(200, toggleLed);
            debugComms.print(INFO, "Request received\r\n");
            confirmDelivery();
            master_fsm_state = ST_REQUEST_PENDING;
        }
        else if (buttonPressed(NEG_BUTTON_PIN)){
            clearLeds();
            master_fsm_state = ST_AVAILABLE;
        }
        else if(!digitalRead(AVAILABILITY_SWITCH)){
            clearLeds();
            digitalWrite(LED_STATUS_UNAVAILABLE, LED_ON);
            digitalWrite(LED_STATUS_AVAILABLE, LED_OFF);
            switchSwitched = true;
            master_fsm_state = ST_UNAVAILABLE;
        }
        break;

    case ST_UNAVAILABLE:
        if(digitalRead(AVAILABILITY_SWITCH) 
            && ((endOfAutoReject || buttonPressed(POS_BUTTON_PIN)) || switchSwitched)){
            if(!endOfAutoReject){
                autoRejectTimer.detach();
            }
            endOfAutoReject = false;
            switchSwitched = false;
            digitalWrite(LED_STATUS_UNAVAILABLE, LED_OFF);
            digitalWrite(LED_STATUS_AVAILABLE, LED_ON);
            master_fsm_state = ST_AVAILABLE;
        }
        else if(reqReceived){
            reqReceived = false;
            rejectRequest();
            debugComms.print(INFO, "Request received. Rejecting.\r\n");
        }
        break;

    /* State, which we enter after sending a message, but before a reception convirmation
        is received from the target device */
    case ST_INVITATION_SENT:

        /* On Entry routine */
        if(stateEntry) {
            stateEntry = false;
            /* Set up a signal for automatic state transition on timeout */
            messageDeliveryTimeoutTimer.once(MESSAGE_DELIVERY_TIMEOUT_S, setDeliveryTimeout);

            /* Indicate state through a fast blinking LED */
            toggleLedTimer.attach_ms(100, toggleLed);

        }

        /* Transitions */
        if(messageDeliveryTimeout) {
            messageDeliveryTimeout = false;
            digitalWrite(LED_INFO, LED_OFF);
            master_fsm_state = ST_REQUEST_FAILED;
        }

        if(msgDeliveryConfirmed) {
            msgDeliveryConfirmed = false;
            digitalWrite(LED_INFO, LED_ON);
            master_fsm_state = ST_AWAITING_RESPONSE;
        }

        /* On exit cleanup routine */
        if(master_fsm_state != ST_INVITATION_SENT) {
            toggleLedTimer.detach();
            messageDeliveryTimeoutTimer.detach();
        }

        break;


    case ST_AWAITING_RESPONSE:
        if (endOfAwaiting || buttonPressed(NEG_BUTTON_PIN)){
            endOfAwaiting = false;
            awaitingResponseTimer.detach();
            cancelRequest();
            digitalWrite(LED_INFO, LED_OFF);
            master_fsm_state = ST_AVAILABLE;
        }

        else if (reqAccepted) {
            awaitingResponseTimer.detach();
            reqAccepted = false;
            digitalWrite(LED_INFO, LED_OFF);
            digitalWrite(LED_REQ_ACCEPTED, LED_ON);
            master_fsm_state = ST_AVAILABLE;
        }
        else if (reqRejected) {
            awaitingResponseTimer.detach();
            reqRejected = false;
            digitalWrite(LED_INFO, LED_OFF);
            digitalWrite(LED_REQ_REJECTED, LED_ON);
            master_fsm_state = ST_AVAILABLE;
        }
        break;

    case ST_REQUEST_PENDING:
        if (reqCancelled){
            reqCancelled = false;
            toggleLedTimer.detach();
            digitalWrite(LED_INFO, LED_OFF);
            master_fsm_state = ST_AVAILABLE;
        }
        else if (buttonPressed(POS_BUTTON_PIN)) {
            acceptRequest();
            toggleLedTimer.detach();
            digitalWrite(LED_INFO, LED_OFF);
            master_fsm_state = ST_AVAILABLE;
        }
        else if (buttonPressed(NEG_BUTTON_PIN)) {
            rejectRequest();
            toggleLedTimer.detach();
            digitalWrite(LED_INFO, LED_OFF);
            autoRejectTimer.once(END_AUTO_REJECT_TIMEOUT_S, endAutoReject);
            master_fsm_state = ST_UNAVAILABLE;
        }
        break;

    case ST_REQUEST_ACCEPTED:
        
        break;

    case ST_REQUEST_FAILED:

        debugComms.print(INFO, "Request failed!\n");
        master_fsm_state = ST_AVAILABLE;

        break;
    default:
        break;
    }
}
