#include "mqtt.h"
#include <AsyncMqttClient.h>
#include <ESP8266WiFi.h>
#include <Ticker.h>
#include "debug_comms.h"

#include "master_fsm.h"

AsyncMqttClient mqttClient;
Ticker mqttReconnectTimer;

/***** MQTT *****/

void connectToMqtt() {
    debugComms.print(DEBUG, "Connecting to MQTT...\r\n");
    mqttClient.connect();
}

void onMqttConnect(bool sessionPresent) {
    debugComms.print(INFO, "Connected to MQTT.\r\n");
    debugComms.print(DEBUG, "Session present: %d \r\b", sessionPresent);
    uint16_t packetIdSub = mqttClient.subscribe("lukigruszka/feeds/guzik-hivemq", 2);
    debugComms.print(DEBUG, "Subscribing at QoS 2, packetId: %d \r\n", packetIdSub);
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason) {
    master_fsm_state = ST_NETWORK_SETUP;
    
    debugComms.print(INFO, "Disconnected from MQTT.\r\n");

    if (reason == AsyncMqttClientDisconnectReason::TLS_BAD_FINGERPRINT) {
        debugComms.print(DEBUG, "Bad server fingerprint.\r\n");
    }

    if (WiFi.isConnected()) {
        mqttReconnectTimer.once(4, connectToMqtt);
    }
}

void onMqttSubscribe(uint16_t packetId, uint8_t qos) {
    debugComms.print(DEBUG, "Subscribe acknowledged.\r\n");
    debugComms.print(DEBUG, "  packetId: %d \r\n", packetId);
	debugComms.print(DEBUG, "\r\n");
    debugComms.print(DEBUG, "  qos: %d \r\n", qos);

    master_fsm_state = ST_AVAILABLE;
}

void onMqttUnsubscribe(uint16_t packetId) {
    debugComms.print(DEBUG, "Unsubscribe acknowledged.\r\n");
    debugComms.print(DEBUG, "  packetId: %d", packetId);
    }

void onMqttMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total) {
    debugComms.print(INFO, "Publish received.\r\n");
    debugComms.print(DEBUG, "  topic: ");
    debugComms.print(DEBUG, topic);
	debugComms.print(DEBUG, "\r\n");
    debugComms.print(DEBUG, "  qos: %d \r\n", properties.qos);
    debugComms.print(DEBUG, "  dup: %d \r\n", properties.dup);
    debugComms.print(DEBUG, "  retain: %d \r\n", properties.retain);
    debugComms.print(DEBUG, "  len: %d \r\n", len);
    debugComms.print(DEBUG, "  index: %d \r\n", index);
    debugComms.print(DEBUG, "  total: %d\r\n", total);

    handleQuery(payload, len);
}

void onMqttPublish(uint16_t packetId) {
    debugComms.print(INFO, "Publish acknowledged.\r\n");
    debugComms.print(DEBUG, "  packetId: %d \r\n", packetId);
}
