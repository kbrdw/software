#include <Arduino.h>
#include <Ticker.h>

#include "core_functions.h"
#include "mqtt.h"
#include "debug_comms.h"
#include "master_fsm.h"

#include "config.h"

Ticker toggleLedTimer;
Ticker autoRejectTimer;
Ticker awaitingResponseTimer;
Ticker messageDeliveryTimeoutTimer;

/* Event flags */
bool reqReceived = false;
bool reqAccepted = false;
bool reqRejected = false;
bool reqCancelled = false;
bool msgDeliveryConfirmed = false;      /* Set when peer sends confirmation of invitation delivery */
bool endOfAutoReject = false;
bool endOfAwaiting = false;
bool switchSwitched = false;
bool messageDeliveryTimeout = false;        /* Set if no confirmation is received within set time */

bool waitingForResponse = false;
int requesterID;

/***** Functional code *****/

void toggleLed(){
    digitalWrite(LED_INFO, !digitalRead(LED_INFO));
}

void clearLeds(){
    digitalWrite(LED_INFO, LED_OFF);
    digitalWrite(LED_REQ_ACCEPTED, LED_OFF);
    digitalWrite(LED_REQ_REJECTED, LED_OFF);
}

bool buttonPressed(int pinNumber){
    static int lastDebounceTime = 0;
    bool newPressEvent = false;

    if(!digitalRead(pinNumber)){
        if(millis() - lastDebounceTime > DEBOUNCE_THRESHOLD_MS){
           newPressEvent = true; 
        }
        lastDebounceTime = millis();
    }

    return newPressEvent;
}

void handleQuery(char* payload_a, size_t len){
    payload_a[len] = '\0';
    String payload_s(payload_a);
    debugComms.print(INFO, "  payload: %s \r\n", payload_s.c_str());

    String requester = payload_s.substring(0, payload_s.indexOf('-'));
    String request = payload_s.substring(payload_s.indexOf('-') + 1);

    requesterID = requester.toInt();
    if(requesterID == MY_ID)
        return;

    if(request == String("req-") + MY_ID && (master_fsm_state == ST_AVAILABLE || master_fsm_state == ST_UNAVAILABLE)){
        reqReceived = true;
    }
    else if(request == String("accept-") + MY_ID && master_fsm_state == ST_AWAITING_RESPONSE)
    {
        reqAccepted = true;
    }
    else if(request == String("reject-") + MY_ID && master_fsm_state == ST_AWAITING_RESPONSE)
    {
        reqRejected = true;
    }
    else if(request == String("cancel-") + MY_ID && master_fsm_state == ST_REQUEST_PENDING)
    {
        reqCancelled = true;
    }
    else if(request == String("confirm-") + MY_ID && master_fsm_state == ST_INVITATION_SENT) {
        msgDeliveryConfirmed = true;
    }
}

void acceptRequest(){
    String payload = String(MY_ID) + String("-accept-") + String(REMOTE_ID);
    uint16_t packetIdPub2 = mqttClient.publish("lukigruszka/feeds/guzik-hivemq", 2, true, payload.c_str());
    debugComms.print(DEBUG, "Publishing at QoS 2, packetId: %d \r\n", packetIdPub2);
}

void sendRequest(){
    String payload = String(MY_ID) + String("-req-") + String(REMOTE_ID);
    uint16_t packetIdPub2 = mqttClient.publish("lukigruszka/feeds/guzik-hivemq", 2, true, payload.c_str());
    debugComms.print(DEBUG, "Publishing at QoS 2, packetId: %d \r\n", packetIdPub2);
}

void cancelRequest(){
    String payload = String(MY_ID) + String("-cancel-") + String(REMOTE_ID);
    uint16_t packetIdPub2 = mqttClient.publish("lukigruszka/feeds/guzik-hivemq", 2, true, payload.c_str());
    debugComms.print(DEBUG, "Publishing at QoS 2, packetId: %d \r\n", packetIdPub2);
}

void rejectRequest(){
    String payload = String(MY_ID) + String("-reject-") + String(REMOTE_ID);
    uint16_t packetIdPub2 = mqttClient.publish("lukigruszka/feeds/guzik-hivemq", 2, true, payload.c_str());
    debugComms.print(DEBUG, "Publishing at QoS 2, packetId: %d \r\n", packetIdPub2);
}

void confirmDelivery() {
    String payload = String(MY_ID) + String("-confirm-") + String(REMOTE_ID);
    uint16_t packetIdPub2 = mqttClient.publish("lukigruszka/feeds/guzik-hivemq", 2, true, payload.c_str());
    debugComms.print(DEBUG, "Publishing at QoS 2, packetId: %d \r\n", packetIdPub2);
}

void endAutoReject(){
    endOfAutoReject = true;
}

void endAwaitingResponse(){
    endOfAwaiting = true;
}

void setDeliveryTimeout() {
    messageDeliveryTimeout = true;
}