#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>

#include "Arduino.h"

#include "debug_comms.h"


/*
    Shamelessly copied from Print.cpp from arduino espertiff framework 
    printf implementation by 2008 David A. Mellis
*/
// higher level -> higher verbosity
// Print all messages below or equal to set level
void DebugComms::print(int level, const char *format, ...) {
    
    if(level <= currentVerbosityLevel) {
        va_list arg;
        va_start(arg, format);
        char temp[64];
        char* buffer = temp;
        size_t len = vsnprintf(temp, sizeof(temp), format, arg);
        va_end(arg);
        if (len > sizeof(temp) - 1) {
            buffer = new char[len + 1];
            if (!buffer) {
                return;
            }
            va_start(arg, format);
            vsnprintf(buffer, len + 1, format, arg);
            va_end(arg);
        }
        
        len = Serial.write((const uint8_t*) buffer, len);
         
        if (buffer != temp) {
            delete[] buffer;
        }
    }
    return;
    
} 

void DebugComms::setLevel(int newLevel) {
    if(newLevel <= MAX_LEVEL && newLevel >= MIN_LEVEL) {
        currentVerbosityLevel = newLevel;
    } else {
        print(DEBUG,"Invalid verbosity level (%d), must be in range [%d, %d]\n",
        newLevel, MIN_LEVEL, MAX_LEVEL);
    }
}

DebugComms debugComms(MAX_LEVEL);