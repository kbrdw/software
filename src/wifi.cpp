#include "wifi.h"
#include "debug_comms.h"

#include "master_fsm.h"

WiFiEventHandler wifiConnectHandler;
WiFiEventHandler wifiDisconnectHandler;
Ticker wifiReconnectTimer;

void connectToWifi() {
    debugComms.print(DEBUG, "Connecting to Wi-Fi... \r\n");
    WiFi.begin(WIFI_SSID, WIFI_PASS);
}

void onWifiConnect(const WiFiEventStationModeGotIP& event) {
    master_fsm_state = ST_NETWORK_SETUP;

    debugComms.print(INFO, "Connected to Wi-Fi. \r\n");
    connectToMqtt();
}

void onWifiDisconnect(const WiFiEventStationModeDisconnected& event) {
    master_fsm_state = ST_WIFI_CONNECT;
    
    debugComms.print(INFO, "Disconnected from Wi-Fi. \r\n");
    mqttReconnectTimer.detach(); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
    wifiReconnectTimer.once(2, connectToWifi);
}